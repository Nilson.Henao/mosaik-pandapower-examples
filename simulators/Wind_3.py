from PySSC import PySSC
import numpy as np
import os

ssc = PySSC()

class Simulator(object):
	def __init__(self):
		self.dirpath = os.getcwd()+ '\data\CA Southwestern-Mountainous.srw'

	def start(self):
		ssc.module_exec_set_print(0)
		data = ssc.data_create()
		ssc.data_set_string(data, b'wind_resource_filename',
							b'data/CA Southern-Mountainous.srw') ;
		ssc.data_set_number(data, b'wind_resource_shear', 0.14000000059604645)
		ssc.data_set_number(data, b'wind_resource_turbulence_coeff', 0.10000000149011612)
		ssc.data_set_number(data, b'system_capacity', 30)
		ssc.data_set_number(data, b'wind_resource_model_choice', 0)
		ssc.data_set_number(data, b'weibull_reference_height', 50)
		ssc.data_set_number(data, b'weibull_k_factor', 2.4000000953674316)
		ssc.data_set_number(data, b'weibull_wind_speed', 6.6999998092651367)
		ssc.data_set_number(data, b'wind_turbine_rotor_diameter', 2.5)
		wind_turbine_powercurve_windspeeds = [0, 3, 3.5999999046325684, 5.4000000953674316, 7.1999998092651367,
											  8.8999996185302734, 10.699999809265137, 12.5, 14.300000190734863,
											  16.100000381469727, 17.899999618530273, 19.700000762939453, 21, 23, 40];
		ssc.data_set_array(data, b'wind_turbine_powercurve_windspeeds', wind_turbine_powercurve_windspeeds);
		wind_turbine_powercurve_powerout = [0, 0, 0.048000000417232513, 0.1550000011920929, 0.38999998569488525,
											0.68999999761581421, 1.0199999809265137, 1.2200000286102295,
											1.190000057220459, 1.0850000381469727, 1, 0.89999997615814209, 0.5, 0, 0];
		ssc.data_set_array(data, b'wind_turbine_powercurve_powerout', wind_turbine_powercurve_powerout);
		ssc.data_set_number(data, b'wind_turbine_hub_ht', 80)
		ssc.data_set_number(data, b'wind_turbine_max_cp', 0.44999998807907104)
		wind_farm_xCoordinates = [0, 20, 40, 60, 80, 0, 20, 40, 60, 80, 0, 20, 40, 60, 80, 0, 20, 40, 60, 80,0, 20, 40, 60, 80, 0, 20, 40, 60, 80];
		ssc.data_set_array(data, b'wind_farm_xCoordinates', wind_farm_xCoordinates);
		wind_farm_yCoordinates = [0, 0, 0, 0, 0, 20, 20, 20, 20, 20, 40, 40, 40, 40, 40, 60, 60, 60, 60, 60,80,80,80,80,80,100,100,100,100,100];
		ssc.data_set_array(data, b'wind_farm_yCoordinates', wind_farm_yCoordinates);
		ssc.data_set_number(data, b'wind_farm_losses_percent', 0)
		ssc.data_set_number(data, b'wind_farm_wake_model', 0)
		ssc.data_set_number(data, b'adjust:constant', 0.5)
		module = ssc.module_create(b'windpower')
		ssc.module_exec_set_print(0);
		if ssc.module_exec(module, data) == 0:
			print('windpower simulation error')
			idx = 1
			msg = ssc.module_log(module, 0)
			while (msg != None):
				print('	: ' + msg.decode("utf - 8"))
				msg = ssc.module_log(module, idx)
				idx = idx + 1
			SystemExit("Simulation Error");
		ssc.module_free(module)
		annual_energy = ssc.data_get_number(data, b'annual_energy');
		# print('Annual energy (year 1) = ', annual_energy)
		capacity_factor = ssc.data_get_number(data, b'capacity_factor');
		# print('Capacity factor (year 1) = ', capacity_factor)
		system_power_generated = ssc.data_get_array(data, b'gen');
		print('System Power Generated = ', np.divide(system_power_generated,1000), len(system_power_generated))
		ssc.data_free(data);

		return  np.divide(system_power_generated,1000)


if __name__ == "__main__":
	Simulator().start()
